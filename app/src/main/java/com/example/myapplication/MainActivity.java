package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.myapplication.util.Constant;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    EditText fname,phno,mail;
    Button submit;
    RadioGroup rggender;
    RadioButton rbmale,rbfemale;
    CheckBox cbcircket,cbfootball,cbhockey;
    ImageView ivclearName, ivclearMobileNo, ivclearEmail;

    ArrayList<HashMap<String, Object>> userlist = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fname=findViewById(R.id.etActfname);
        phno=findViewById(R.id.etActpno);
        mail=findViewById(R.id.etActmai);
        submit=findViewById(R.id.btnActsub);
        rggender=findViewById(R.id.rgActgender);
        rbfemale=findViewById(R.id.rbActfemale);
        rbmale=findViewById(R.id.rbActmale);
        cbcircket=findViewById(R.id.cbActcricket);
        cbfootball=findViewById(R.id.cbActfootball);
        cbhockey=findViewById(R.id.cbActhockey);
        ivclearName = findViewById(R.id.ivActclearName);
        ivclearMobileNo = findViewById(R.id.ivActclearMobileNumber);
        ivclearEmail = findViewById(R.id.ivActclearEmail);

        clearScreen();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()){

                    HashMap<String, Object> map = new HashMap<>();
                    map.put(Constant.FIRST_NAME, fname.getText().toString());
                    map.put(Constant.MOBILE_NUMBER, phno.getText().toString());
                    map.put(Constant.EMAIL_ADDRESS, mail.getText().toString());
                    map.put(Constant.GENDER, rbmale.isChecked() ? rbmale.getText().toString() : rbfemale.getText().toString());

                    String Hobby = "";

                    if (cbcircket.isChecked()) {
                        Hobby += "," + cbcircket.getText().toString();

                    }
                    if (cbfootball.isChecked()) {
                        Hobby += "," + cbfootball.getText().toString();
                    }
                    if (cbhockey.isChecked()) {
                        Hobby += "," + cbhockey.getText().toString();
                    }

                    Hobby.replaceFirst(String.valueOf(Hobby.charAt(0)), "");

                    map.put(Constant.HOBBY, Hobby);
                    userlist.add(map);

                    Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                    intent.putExtra("UserList", userlist);
                    startActivity(intent);
                    fname.setText("");
                    phno.setText("");
                    mail.setText("");

                }


            }
        });

        if(rbfemale.isChecked())
        {
            cbcircket.setVisibility(View.VISIBLE);
            cbfootball.setVisibility(View.VISIBLE);
            cbhockey.setVisibility(View.GONE);
        }

        rggender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                if(i==R.id.rbActmale){
                    cbcircket.setVisibility(View.VISIBLE);
                    cbfootball.setVisibility(View.VISIBLE);
                    cbhockey.setVisibility(View.VISIBLE);
                }
                else
                {
                    cbcircket.setVisibility(View.VISIBLE);
                    cbfootball.setVisibility(View.VISIBLE);
                    cbhockey.setVisibility(View.GONE);

                }
            }
        });

    }

    boolean isValid(){
        boolean flag=true;
        if(TextUtils.isEmpty(fname.getText().toString().trim()))
        {
            fname.setError("please enter your name");
            flag=false;
        }
        else
        {
            String firstname=fname.getText().toString().trim();
            String pattern="^[a-zA-Z]+";
            if(!firstname.matches(pattern)){
                fname.setError("please enter valid name");
                flag=false;

            }
        }


        if(TextUtils.isEmpty(mail.getText().toString().trim()))
        {
            mail.setError("please enter your email");
            flag=false;
        }
        else
        {
            String email=mail.getText().toString().trim();
            String pattern="^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            if(!email.matches(pattern)){
                mail.setError("please enter valid email");
                flag=false;

            }
        }


        if(TextUtils.isEmpty(phno.getText().toString().trim()))
        {
            phno.setError("please enter your phone number");
            flag=false;
        }
        else
        {
            if (phno.getText().toString().trim().length()<10){
                phno.setError("please enter valid phone number");
                flag=false;

            }
        }

        return flag;
    }





    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }


    void clearScreen() {
        fname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {
                    ivclearName.setVisibility(View.VISIBLE);
                    ivclearName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            fname.setText("");
                        }
                    });
                } else {
                    ivclearName.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        phno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {
                    ivclearMobileNo.setVisibility(View.VISIBLE);
                    ivclearMobileNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            phno.setText("");
                        }
                    });
                } else {
                    ivclearMobileNo.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {
                    ivclearEmail.setVisibility(View.VISIBLE);
                    ivclearEmail.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mail.setText("");
                        }
                    });
                } else {
                    ivclearEmail.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

}


